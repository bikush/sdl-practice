# sdl-practice
Repository for some SDL2 practice

## Linux build

Install cmake 3.7 or later. That might be difficult, so you could always build [from source](https://askubuntu.com/questions/355565/how-do-i-install-the-latest-version-of-cmake-from-the-command-line) - steps 1,3,4. Get the latest source [here](https://cmake.org/download/).

Get SDL2 by running `sudo apt get install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev`.

When thats done, in this repository:

```
cd projects/
cmake ../source/
make
```

It will produce an executable with the name that matches whatever is at the time in `CMakeLists/source.txt`

## Windows build

Get latest CMake from the cmake website.
SDL2 comes with this repository. Version 2.0.8, located in the `external` folder.
Use CMake gui to generate the project in the `projects/` folder.

