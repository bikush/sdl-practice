#include <string>

struct SDL_Surface;
struct SDL_Texture;
struct SDL_Renderer;
struct _TTF_Font;
typedef struct _TTF_Font TTF_Font;

class Label {
public:
	Label(int x, int y):
		posX{ x }, posY{ y }, text{
		""
	}, textSurface{ nullptr }, textTexture{ nullptr } {}
	~Label();
	void render(SDL_Renderer * renderer, TTF_Font * font, const std::string & text, int x, int y);
	const std::string & getText() { return text; }
	SDL_Surface* getSurface() { return textSurface; }
	SDL_Texture* getTexture() { return textTexture; }
private:
	std::string text;
	SDL_Surface* textSurface;
	SDL_Texture* textTexture;
	int posX;
	int posY;
	void updateLabel(SDL_Renderer* renderer, TTF_Font *font, const std::string & txt);
};