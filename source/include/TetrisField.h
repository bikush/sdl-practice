#pragma once
#include "TextureCache.h"
#include "TetrisUtils.h"
#include "TetrisLogic.h"
#include <SDL.h>

class TetrisField {
public:
	TetrisField() : 
		logic{ tetris::TETRIS_FIELD_UPPER_LEFT_X ,
			   tetris::TETRIS_FIELD_UPPER_LEFT_Y,
			   tetris::TETRIS_FIELD_WIDTH,
			   tetris::TETRIS_FIELD_HIGHT },
		points{ 0 },
		topScore{ 0 },
		blockMovedDownTime{ 0 },
		alreadyMovedDown{false}
	{
		readTopScore();
		logic.startANewGame();
	}

	~TetrisField() { writeTopScore(); }

	void onKeyEvent(SDL_Keycode keyCode);
	bool moveTetrisBlockDown(unsigned int currentTime);
	void removeFullLines();
	bool addNewTetrisBlock();
	void render(SDL_Renderer* renderer, const TextureCache & tc);
	int getPoints() { return points; }
	int getTopScore() { return topScore;  }
	tetris::TetrisID getNextBlockId() { return logic.getNextBlockId(); }
	void reStartGame();

private:
	int calculateSpeed();
	void readTopScore();
	void writeTopScore();
	
	TetrisLogic  logic;
	int points;
	int topScore;
	unsigned int blockMovedDownTime;
	bool alreadyMovedDown;
	
};

