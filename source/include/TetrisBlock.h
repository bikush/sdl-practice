#pragma once
#include <vector>
#include "TetrisUtils.h"
#include "FieldElement.h"

class TetrisBlock {

public:

	TetrisBlock(tetris::TetrisID tId, const std::vector<std::pair<int, int>> & indexes):
		id{tId},
		blockIndexes { blockIndexes } {}
	const std::vector<std::pair<int, int>> & getIndexes() { return blockIndexes; }
	tetris::TetrisID getId() { return id; }
	void setIndexes(const std::vector<std::pair<int, int>> & indexes) { blockIndexes = indexes; }
	void setId(tetris::TetrisID tId) { id = tId;  }

private:

	std::vector<std::pair<int, int>> blockIndexes;
	tetris::TetrisID id;
};

