#pragma once
#include "TetrisUtils.h"

struct fieldElement {
	tetris::TetrisID id;
	const int xPos;
	const int yPos;
	const int width;

	fieldElement(int ulx, int uly, int w, int xIndex, int yIndex, tetris::TetrisID tId) :
		width{ w }, id{ tId }, xPos{ ulx + width * xIndex }, yPos{ uly + width * yIndex } { }
};


