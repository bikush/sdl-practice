#pragma once
#include "TetrisField.h"
#include "TextureCache.h"
#include "NextBlockWidget.h"
#include "Label.h"
#include <iostream>

struct SDL_Window;
struct SDL_Renderer;
struct _TTF_Font;
typedef struct _TTF_Font TTF_Font;
struct SDL_Rect;
struct SDL_Texture;

class GameApp {

public:

	GameApp();
	int run();

private:

	void init();
	void update();
	void render();
	void cleanup();
	void loadTexture(const std::string & path);

	TTF_Font *font;
	SDL_Window* screen;
	SDL_Renderer* renderer;
	SDL_Rect gameFieldPos;
	TetrisField  tetrisField;
	NextBlockWidget nextBlock;
	TextureCache tc;
	Label gameEndLabel;
	Label pointsLabel;
	Label topScoreLabel;
	bool gameEndScreen;
	bool running;
};

