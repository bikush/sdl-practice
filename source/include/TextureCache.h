#pragma once
#include <map>

struct SDL_Texture;
struct SDL_Renderer;

class TextureCache {
public:
	TextureCache() {}
	~TextureCache();
	bool loadImageTexture(SDL_Renderer* renderer, const std::string & path);
	SDL_Texture* getImageTexture(const std::string & path) const;
private:
	std::map<std::string, SDL_Texture*> imageTextures;
};

