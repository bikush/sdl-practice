#pragma once
#include <map>
#include "TetrisUtils.h"
#include "FieldElement.h"

struct SDL_Renderer;
class TextureCache;

class NextBlockWidget {

public:

	NextBlockWidget(int upperLeftX, int upperLeftY, int width);
	const std::map<std::pair<int, int>, fieldElement> & getNextBlockField() { 
		return nextBlockField; 
	}
	void updateWidget(tetris::TetrisID id);
	void render(SDL_Renderer* renderer, const TextureCache & tc);
private:

	std::map<std::pair<int, int>, fieldElement> nextBlockField;
};