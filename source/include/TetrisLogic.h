#pragma once
#include <map>
#include <vector>
#include <string>
#include "TetrisUtils.h"
#include "FieldElement.h"
#include "TetrisBlock.h"

class TetrisLogic {
public:
	enum class DIR {
		DOWN,
		LEFT,
		RIGHT,
		ROTATE
	};

	TetrisLogic(int upperLeftX, int upperLeftY, int width, int height);
	const std::map<std::pair<int, int>, fieldElement> & getTetrisField();
	void startANewGame();
	int removeFullLines();
	void updatePosition(TetrisLogic::DIR pos = TetrisLogic::DIR::DOWN);
	int getNumberOfColumns() { return numberOfColumns; }
	bool hasTheGameEnded();
	bool hasBlockLanded();
	tetris::TetrisID getNextBlockId() { return nextBlockId; }
	void createCurrTetrisBlock();

private:

	std::map<std::pair<int, int>, fieldElement> tetrisField;
	TetrisBlock currTetrisBlock;
	int numberOfRows;
	int numberOfColumns;
	int upperLeftX, upperLeftY;
	int fieldElemWidth;
	bool blockLanded;
	tetris::TetrisID nextBlockId;
	
	void removeRows(const std::vector<int> & rows);
	void rotateAntiClockWise();
	std::vector<std::pair<int, int>> getNextShapeIndexes();
};
