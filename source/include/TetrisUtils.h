#pragma once
#include <string>
#include <map>

namespace tetris {
	enum class TetrisID {
		EMPTY = -1,
		SQUARE,
		LINE,
		L_SHAPE,
		MIRR_L_SHAPE,
		Z_SHAPE,
		S_SHAPE,
		T_SHAPE
	};

	std::string getShape(tetris::TetrisID tId);
	int getPivot(tetris::TetrisID tId);
	tetris::TetrisID generateRandomId();

	const std::string BACKGROUND = "media/Mockup/background.jpg";
	const std::string T_ELEMENT = "media/T/T_element.png";
	const std::string I_ELEMENT = "media/I/I_element.png";
	const std::string L_ELEMENT = "media/L/L_element.png";
	const std::string R_ELEMENT = "media/R/R_element.png";
	const std::string S_ELEMENT = "media/S/S_element.png";
	const std::string TOP_SCORE = "top_score.txt";

	const std::map<TetrisID, std::string> tetrisElemTexturePath = {
		{TetrisID::LINE, I_ELEMENT},
		{TetrisID::L_SHAPE, L_ELEMENT},
		{TetrisID::MIRR_L_SHAPE, L_ELEMENT},
		{TetrisID::SQUARE, R_ELEMENT},
		{TetrisID::S_SHAPE, S_ELEMENT},
		{TetrisID::Z_SHAPE, S_ELEMENT},
		{TetrisID::T_SHAPE, T_ELEMENT}
	};

	const int SCREEN_WIDTH = 520;
	const int SCREEN_HEIGHT = 630;
	const int TETRIS_FIELD_UPPER_LEFT_X = 15;
	const int TETRIS_FIELD_UPPER_LEFT_Y = 62;
	const int TETRIS_FIELD_WIDTH = 355;
	const int TETRIS_FIELD_HIGHT = 556;
	const int NEXT_WIDGET_UPPER_LEFT_X = 390;
	const int NEXT_WIDGET_UPPER_LEFT_Y = 205;
	const int NEXT_WIDGET_WIDTH = 27;
	const std::string FONT = "media/Font/telelower.ttf";
	const int FONT_SIZE = 18;
	const int POINTS_X = 415;
	const int POINTS_Y = 77;
	const int TOP_SCORE_X = 415;
	const int TOP_SCORE_Y = 135;
	const std::string GAME_END_MSG = "Press any key to continue ...";
	const int GAME_END_X = 20;
	const int GAME_END_Y = 68;
}

