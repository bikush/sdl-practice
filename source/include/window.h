#pragma once

struct SDL_Window;
struct SDL_Renderer;

class Window {
	SDL_Window* m_window;
	SDL_Renderer* m_renderer;

public:
	Window();
	~Window();

    void Work();
};
