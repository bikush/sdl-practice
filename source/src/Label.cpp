#include "Label.h"
#include <SDL.h>
#include <SDL_ttf.h>

Label::~Label() {
	if (textSurface != nullptr) {
		SDL_FreeSurface(textSurface);
	}
	if (textTexture != nullptr) {
		SDL_DestroyTexture(textTexture);
	}
}

void Label::render(SDL_Renderer * renderer, TTF_Font * font, const std::string & text, int x, int y) {
	if (getText() != text) {
		updateLabel(renderer, font, text);
	}
	if (textSurface != nullptr &&
		textSurface != nullptr) {

		SDL_Rect r = {
			x,
			y,
			textSurface->w,
			textSurface->h
		};
		SDL_RenderCopy(renderer, textTexture, NULL, &r);
	}
}

void Label::updateLabel(SDL_Renderer* renderer, TTF_Font *font, const std::string & txt) {
	text = txt;
	SDL_Color c = { 242, 227, 182, 255 };
	SDL_Surface* surface = TTF_RenderText_Solid(font,
		text.c_str(),
		c);
	
	if (surface != nullptr) {
		SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
		if (texture != nullptr) {
			if (textSurface != nullptr) {
				SDL_FreeSurface(textSurface);
			}
			textSurface = surface;
			if (textTexture != nullptr) {
				SDL_DestroyTexture(textTexture);
			}
			textTexture = texture;
		} else {
			SDL_FreeSurface(surface);
		}
		
	}

}
