#include "TextureCache.h"
#include <SDL.h> 
#include <SDL_image.h>


using namespace std;

TextureCache::~TextureCache() {
	for (auto elem : imageTextures) {
		SDL_DestroyTexture(elem.second);
	}
}

bool TextureCache::loadImageTexture(SDL_Renderer* renderer, const std::string & path) {
	SDL_Texture* texture = nullptr;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface) {
		texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		SDL_FreeSurface(loadedSurface);
	}
	imageTextures.insert(make_pair(path, texture));
	return texture != nullptr;
}

SDL_Texture * TextureCache::getImageTexture(const std::string & path) const {
	return imageTextures.at(path);
}
