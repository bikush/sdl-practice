#include "NextBlockWidget.h"
#include "TextureCache.h"
#include <SDL.h>

using namespace std;
using namespace tetris;

NextBlockWidget::NextBlockWidget(int upperLeftX, int upperLeftY, int width) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++) {
			fieldElement elem{upperLeftX, upperLeftY, width, i, j, TetrisID::EMPTY };
			nextBlockField.insert(make_pair(make_pair(i, j), elem));
		}
	}
}

void NextBlockWidget::updateWidget(TetrisID id) {
	const string shape = getShape(id);
	pair<int, int> idx = make_pair(0, 0);
	for (auto & elem : nextBlockField) {
		elem.second.id = TetrisID::EMPTY;
	}
	for (const auto c : shape) {
		fieldElement & elem = nextBlockField.at(idx);
		if (c == 'x') {
			elem.id = id;
		}
		else if (c == '\n') {
			idx.first = 0;
			idx.second = 1;
			continue;
		}
		idx.first += 1;
	}
}

void NextBlockWidget::render(SDL_Renderer* renderer, const TextureCache & tc) {
	for (const auto & elem : nextBlockField) {
		if (elem.second.id != TetrisID::EMPTY) {
			SDL_Rect r{ elem.second.xPos,
				elem.second.yPos,
				elem.second.width,
				elem.second.width };
			const string path = tetrisElemTexturePath.at(elem.second.id);
			SDL_RenderCopy(renderer, tc.getImageTexture(path), NULL, &r);
		}
	}
}
