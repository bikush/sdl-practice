#include <vector>
#include <random>
#include "TetrisUtils.h"

using namespace std;
using namespace tetris;

string tetris::getShape(TetrisID tId) {
	const vector<string> v{
	"oxx\noxx",    // TetrisID::SQUARE
	"xxxx",        // TetrisID::LINE
	"xxx\nx",      // TetrisID::L_SHAPE
	"xxx\noox",    // TetrisID::MIRR_L_SHAPE
	"xx\noxx",     // TetrisID::Z_SHAPE
	"oxx\nxx",     // TetrisID::S_SHAPE
	"ox\nxxx"      // TetrisID::T_SHAPE
	};
	return v[static_cast<int>(tId)];
}

int tetris::getPivot(TetrisID tId) {
	const vector<int> pivotPoints{ -1, 1, 1, 1, 2, 3, 2 };
	return pivotPoints[static_cast<int>(tId)];
}

TetrisID tetris::generateRandomId() {
	static std::random_device rd;
	static std::mt19937 gen(rd());
	static std::uniform_int_distribution<> dis(0, 6);
	return static_cast<TetrisID>(dis(gen));
}