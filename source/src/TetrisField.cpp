#include "TetrisField.h"
#include <fstream>

using namespace std;
using namespace tetris;

void TetrisField::onKeyEvent(SDL_Keycode keyCode) {
	switch (keyCode) {
	case SDLK_DOWN:
		logic.updatePosition(TetrisLogic::DIR::DOWN);
		alreadyMovedDown = true;
		break;
	case SDLK_LEFT:
		logic.updatePosition(TetrisLogic::DIR::LEFT);
		break;
	case SDLK_RIGHT:
		logic.updatePosition(TetrisLogic::DIR::RIGHT);
		break;
	case SDLK_UP:
		logic.updatePosition(TetrisLogic::DIR::ROTATE);
		break;
	}
}

bool TetrisField::moveTetrisBlockDown(unsigned int currentTime) {
	const int speed = calculateSpeed();
	if (!alreadyMovedDown &&
		(currentTime > (blockMovedDownTime + speed) ||
			blockMovedDownTime > currentTime)) {
		blockMovedDownTime = currentTime;
		logic.updatePosition(TetrisLogic::DIR::DOWN);
	}
	alreadyMovedDown = false;
	return !(logic.hasBlockLanded());
}

void TetrisField::removeFullLines() {
	const int numLines = logic.removeFullLines();
	points += numLines * logic.getNumberOfColumns();
}

bool TetrisField::addNewTetrisBlock() {
	if (!logic.hasTheGameEnded()) {
		logic.createCurrTetrisBlock();
		return false;
	} 
	return true;
}

void TetrisField::render(SDL_Renderer * renderer, const TextureCache & tc) {
	for (const auto & elem : logic.getTetrisField()) {
		if (elem.second.id != TetrisID::EMPTY) {
			SDL_Rect r{ elem.second.xPos,
				elem.second.yPos,
				elem.second.width,
				elem.second.width };
			const string path = tetrisElemTexturePath.at(elem.second.id);
			SDL_RenderCopy(renderer, tc.getImageTexture(path), NULL, &r);
		}
	}
}

void TetrisField::reStartGame() {
	topScore = topScore < points ? points : topScore;
	points = 0;
	logic.startANewGame();
}

int TetrisField::calculateSpeed() {
	return points < 1000 ? ((-points * 4 / 10) + 500) : 100;
}

void TetrisField::readTopScore() {
	ifstream ifs{ TOP_SCORE};
	if (!ifs) {
		writeTopScore();
	} else {
		ifs >> topScore;
	}
}

void TetrisField::writeTopScore() {
	ofstream ofs{ TOP_SCORE, ofstream::out };
	topScore = topScore < points ? points : topScore;
	if (ofs) {
		ofs << topScore << endl;
	}
}
