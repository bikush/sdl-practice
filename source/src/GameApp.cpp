#include "GameApp.h"
#include <SDL.h> 
#include <SDL_image.h>
#include <SDL_ttf.h>

using namespace std;
using namespace tetris;

GameApp::GameApp() : 
	nextBlock{ NEXT_WIDGET_UPPER_LEFT_X,
			   NEXT_WIDGET_UPPER_LEFT_Y,
			   NEXT_WIDGET_WIDTH },
	pointsLabel{ POINTS_X, POINTS_Y },
	topScoreLabel{ TOP_SCORE_X, TOP_SCORE_Y },
    gameEndLabel{ GAME_END_X, GAME_END_Y },
	running{true},
	screen{nullptr},
	renderer{nullptr},
	font{nullptr},
	gameEndScreen{false} { 
}


int GameApp::run() {
	init();
	while (running) {
		update();
		render();
	}
	cleanup();
	return 0;
}

void GameApp::init() {
	nextBlock.updateWidget(tetrisField.getNextBlockId());
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		running = false;
	} else {
		// set texture filtering to linear
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
		screen = SDL_CreateWindow("Tetris",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			SCREEN_WIDTH,
			SCREEN_HEIGHT,
			SDL_WINDOW_SHOWN);
		running = screen != nullptr;
		
		running = running && !(!TTF_WasInit() && TTF_Init() < 0);
		font = running ? TTF_OpenFont(FONT.c_str(), FONT_SIZE): nullptr;
		running = running &&  font != nullptr;
		
		if (running) {
			renderer = SDL_CreateRenderer(
				screen,
				-1,
				SDL_RENDERER_ACCELERATED
			);
			running = running && (renderer != nullptr);

			if (running) {
				SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
				running = running && (IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG);
			}
		}

		loadTexture(BACKGROUND);
		loadTexture(R_ELEMENT);
		loadTexture(L_ELEMENT);
		loadTexture(I_ELEMENT);
		loadTexture(S_ELEMENT);
		loadTexture(T_ELEMENT);
	}
}

void GameApp::loadTexture(const string & path) {
	if (running) {
		running = tc.loadImageTexture(renderer, path);
	}
}

void GameApp::update() {
	SDL_Event e;
	int mouseX = 0;
	int mouseY = 0;

	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT) {
			running = false;
		} else if (e.type == SDL_MOUSEBUTTONDOWN) {
			SDL_GetMouseState(&mouseX, &mouseY);
			cout << "Pixels: X = " << mouseX << " Y = " << mouseY << endl;
		} else if (e.type == SDL_KEYDOWN) {
			if (gameEndScreen == true) {
				gameEndScreen = false;
				tetrisField.reStartGame();
				nextBlock.updateWidget(tetrisField.getNextBlockId());
			} else {
				tetrisField.onKeyEvent(e.key.keysym.sym);
			}
		}
	}
	
	if (!tetrisField.moveTetrisBlockDown(SDL_GetTicks())) {
		tetrisField.removeFullLines();
		gameEndScreen = tetrisField.addNewTetrisBlock();
		nextBlock.updateWidget(tetrisField.getNextBlockId());
	}
}

void GameApp::render() {
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, tc.getImageTexture(BACKGROUND), NULL, NULL);
	tetrisField.render(renderer, tc);
	nextBlock.render(renderer, tc);
	int points = tetrisField.getPoints();
	pointsLabel.render(renderer, font, to_string(points), POINTS_X, POINTS_Y);
	int top_score = tetrisField.getTopScore();
	topScoreLabel.render(renderer, font, to_string(top_score), TOP_SCORE_X, TOP_SCORE_Y);
	if (gameEndScreen) {
		gameEndLabel.render(renderer, font, GAME_END_MSG, GAME_END_X, GAME_END_Y);
	}
	SDL_RenderPresent(renderer);
}

void GameApp::cleanup() {
	TTF_CloseFont(font);
	TTF_Quit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(screen);
	IMG_Quit();
	SDL_Quit();
}
