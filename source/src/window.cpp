#include "window.h"

#include "SDL.h" 

Window::Window() {
	// TODO: move this out if want more windows
	SDL_Init(SDL_INIT_VIDEO);

	m_window = SDL_CreateWindow(
		"A Window",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,
		480,
		0
	);

	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_SOFTWARE);
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(m_renderer);
	SDL_RenderPresent(m_renderer);
}

Window::~Window() {
	SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

void Window::Work() {
	//SDL_Delay(3000);
	SDL_Event event;
	while ( SDL_WaitEvent(&event) >= 0 ) {
		if (event.type == SDL_QUIT)
			break;
	}
}

