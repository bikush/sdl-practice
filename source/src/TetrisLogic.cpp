#include "TetrisLogic.h"
#include <algorithm>
#include <functional>

using namespace std;
using namespace tetris;

TetrisLogic::TetrisLogic(int upperLeftX, int upperLeftY, int width, int height) : 
	currTetrisBlock{ TetrisID::EMPTY, vector<pair<int,int>>{} },
	numberOfColumns{12},
	upperLeftX { upperLeftX },
	upperLeftY{ upperLeftY },
	blockLanded{false} {

	fieldElemWidth = width / numberOfColumns;
	numberOfRows = height / fieldElemWidth;
}

void TetrisLogic::startANewGame() {
	tetrisField.clear();
	for (int i = 0; i < numberOfColumns; i++) {
		for (int j = 0; j < numberOfRows; j++) {
			fieldElement elem{ upperLeftX, upperLeftY, fieldElemWidth, i, j, TetrisID::EMPTY };
			tetrisField.insert(make_pair(make_pair(i, j), elem));
		}
	}
	nextBlockId = generateRandomId(); // the first block id!
	createCurrTetrisBlock();
}

const map<pair<int, int>, fieldElement> & TetrisLogic::getTetrisField() {
	return tetrisField;
}

void TetrisLogic::rotateAntiClockWise() {
	//find rotation point and set angle to 90 degrees
	const int pivotIndex = getPivot(currTetrisBlock.getId());
	vector<pair<int, int>> currBlockIndexes = currTetrisBlock.getIndexes();
	if (pivotIndex != -1) {
		const pair<int, int> centre = currBlockIndexes.at(pivotIndex);
		vector<pair<int, int>> targetIndexes;
		vector<pair<int, int>> newIndexes;
		pair<int, int> targetPoint;
		bool canMove = true;
		for (auto idx : currBlockIndexes) {
			// subtract pivot
			targetPoint.first = idx.first - centre.first;
			targetPoint.second = idx.second - centre.second;
			// rotate point X-> p.x * cos(alpha) - p.y * sin(alpha);
			//              y-> p.x * sin(alpha) + p.y * cos(alpha);
			int tmp = targetPoint.first;
			targetPoint.first = -targetPoint.second;
			targetPoint.second = tmp;
			// translate point back:
			targetPoint.first = targetPoint.first + centre.first;
			targetPoint.second = targetPoint.second + centre.second;
			if (!(targetPoint.first >= 0 && targetPoint.first < numberOfColumns &&
				targetPoint.second >= 0 && targetPoint.second < numberOfRows)) {
				
				canMove = false;
				break;
			}
			else {
				newIndexes.push_back(targetPoint);
				if (find(currBlockIndexes.begin(),
					currBlockIndexes.end(),
					targetPoint) == currBlockIndexes.end()) {
					targetIndexes.push_back(targetPoint);
				}
			}
		}
		if (canMove == true) {
			for (const auto & idx : targetIndexes) {
				if (tetrisField.at(idx).id != TetrisID::EMPTY) {
					canMove = false;
					break;
				}
			}
		}
		if (canMove == true) {
			for (const auto & idx : currBlockIndexes) {
				tetrisField.at(idx).id = TetrisID::EMPTY;
			}
			currBlockIndexes = newIndexes;
			for (const auto & idx : currBlockIndexes) {
				tetrisField.at(idx).id = currTetrisBlock.getId();
			}
		}
	}
	currTetrisBlock.setIndexes(currBlockIndexes);
}

void TetrisLogic::removeRows(const vector<int> & rows) {
	for (int row : rows) {
		for (int j = row; j > 0; j--) {
			for (int i = 0; i < numberOfColumns; i++) {
				pair<int, int> idx1 = make_pair(i, j);
				pair<int, int> idx2 = make_pair(i, j-1);
				tetrisField.at(idx1).id = tetrisField.at(idx2).id;
				tetrisField.at(idx2).id = TetrisID::EMPTY;
			}
		}
	}
}

int TetrisLogic::removeFullLines() {
	vector<int> rowsToRemove;
	for (int j = 0; j < numberOfRows; j++) {
		bool fullLine = true;
		for (int i = 0; i < numberOfColumns; i++) {
			pair<int, int> idx{ i,j };
			if (tetrisField.at(idx).id == TetrisID::EMPTY) {
				fullLine = false;
				break;
			}
		}
		if (fullLine) {
			rowsToRemove.push_back(j);
		}
	}
	removeRows(rowsToRemove);
	return rowsToRemove.size();
}

void TetrisLogic::updatePosition(DIR pos) {
	static function<bool(int, int)> moveDownLeftOrRight = [&](int offsetX, int offsetY) {
		bool canMove = true;
		vector<pair<int, int>> currBlockIndexes = currTetrisBlock.getIndexes();
		for (const auto & idx : currBlockIndexes) {
			pair<int, int> targetIdx = make_pair(idx.first + offsetX, 
												 idx.second + offsetY);
			if (targetIdx.first < 0 ||
				targetIdx.first == numberOfColumns ||
				targetIdx.second == numberOfRows) {
					
				canMove = false;
				break;
			}

			const auto it = find(currBlockIndexes.begin(),
								 currBlockIndexes.end(),
								 targetIdx);
			if (it == currBlockIndexes.end()) {
				fieldElement & elem = tetrisField.at(targetIdx);
				if (elem.id != TetrisID::EMPTY) {
					canMove = false;
					break;
				}
			}
		}

		if (canMove == true) {
			for (auto & idx : currBlockIndexes) {
				tetrisField.at(idx).id = TetrisID::EMPTY;
				idx.first += offsetX;
				idx.second += offsetY;
			}
			for (const auto & idx : currBlockIndexes) {
				tetrisField.at(idx).id = currTetrisBlock.getId();
			}
		}
		currTetrisBlock.setIndexes(currBlockIndexes);
		return canMove;
	};

	switch (pos) {
	case DIR::DOWN:
		blockLanded = !moveDownLeftOrRight(0, 1);
		break;
	case DIR::LEFT:
		moveDownLeftOrRight(-1, 0);
		break;
	case DIR::RIGHT:
		moveDownLeftOrRight(1, 0);
		break;
	case DIR::ROTATE:
		rotateAntiClockWise();
		break;
	}
}

void TetrisLogic::createCurrTetrisBlock() {
	vector<pair<int, int>> currBlockIndexes;
	string shape = getShape(nextBlockId);
	pair<int, int> idx = make_pair(numberOfColumns / 2 - 2, 0);
	blockLanded = false;
	for (const auto c : shape) {
		fieldElement & elem = tetrisField.at(idx);
		if (c == 'x') {
			elem.id = nextBlockId;
			currBlockIndexes.push_back(idx);
		} else if (c == '\n') {
			idx.first = numberOfColumns / 2 - 2;
			idx.second = 1;
			continue;
		}
		idx.first += 1;
	}	
	currTetrisBlock.setId(nextBlockId);
	currTetrisBlock.setIndexes(currBlockIndexes);
	nextBlockId = generateRandomId();
}

bool TetrisLogic::hasBlockLanded() {
	return blockLanded;
}

vector<pair<int,int>> TetrisLogic::getNextShapeIndexes() {
	string shape = getShape(nextBlockId);
	vector<pair<int, int>> res;
	pair<int, int> idx{ numberOfColumns / 2 - 2, 0};
	for (const auto c : shape) {
		if (c == 'x') {
			res.push_back(idx);
		} else if (c == '\n') {
			idx.first = numberOfColumns / 2 - 2;
			idx.second = 1;
			continue;
		}
		idx.first += 1;
	}
	return res;
}

bool TetrisLogic::hasTheGameEnded() {
	//check if there is something on positions
	//where the next shape should be drawn
	bool gameEnded = false;
	for (const auto & idx: getNextShapeIndexes()) {
		if (tetrisField.at(idx).id != TetrisID::EMPTY){
			gameEnded = true;
			break;
		}
	}
	return gameEnded;
}
